package cl.ubb.factorial;

import static org.junit.Assert.*;

import org.junit.Test;

public class FactorialTest {

	@Test
	public void CalcularFactorialIngresoCeroRetornoCero() {
		
		/*arrange*/
		factorial fact = factorial();
		int resultado;
		
		
		/*act*/
		resultado = fact.generaFactorial(0);
		
		
		
		/*assert*/
		
		assertEquals(resultado, 0);
		
	}

}
